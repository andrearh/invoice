package com.training.microservices.arh.invoice.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.training.microservices.arh.invoice.entity.Customer;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
