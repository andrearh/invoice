package com.training.microservices.arh.invoice.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.training.microservices.arh.invoice.entity.Invoice;

public interface InvoiceDao extends PagingAndSortingRepository<Invoice, String> {
}
